import * as THREE from 'three';

export const Objects = () => {

    const objectMaterial = new THREE.MeshStandardMaterial( { color: 0xffffff, roughness: 0.3, metalness: 1.0 } );
    const objectGeometry = new THREE.TorusBufferGeometry( 1.5, 0.4, 8, 16 );
    const meshObjects = [];

    for ( var i = 0; i < 5000; i ++ ) {
      var mesh = new THREE.Mesh( objectGeometry, objectMaterial );
      mesh.position.x = 400 * ( 0.5 - Math.random() );
      mesh.position.y = 50 * ( 0.5 - Math.random() ) + 25;
      mesh.position.z = 200 * ( 0.5 - Math.random() );
      mesh.rotation.y = 3.14 * ( 0.5 - Math.random() );
      mesh.rotation.x = 3.14 * ( 0.5 - Math.random() );
      mesh.matrixAutoUpdate = false;
      mesh.updateMatrix();
      meshObjects.push(mesh)
    }
    return meshObjects
  };
