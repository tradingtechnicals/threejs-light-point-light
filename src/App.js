import React, { Component } from 'react';
import * as THREE from 'three';
import { Objects } from './functions/objects';
// import OrbitControls from './third_party/THREE.OrbitControls.js';
import { FirstPersonControls } from './third_party/FirstPersonControls';

var clock = new THREE.Clock();

class App extends Component {

   mainLoop = () => {
        
    var time = Date.now() * 0.00025;
    var d = 150;
    this.light1.position.x = Math.sin( time * 0.7 ) * d;
    this.light1.position.z = Math.cos( time * 0.3 ) * d;
    this.light2.position.x = Math.cos( time * 0.3 ) * d;
    this.light2.position.z = Math.sin( time * 0.7 ) * d;
    this.light3.position.x = Math.sin( time * 0.7 ) * d;
    this.light3.position.z = Math.sin( time * 0.5 ) * d;
    this.light4.position.x = Math.sin( time * 0.3 ) * d;
    this.light4.position.z = Math.sin( time * 0.5 ) * d;
    this.light5.position.x = Math.cos( time * 0.3 ) * d;
    this.light5.position.z = Math.sin( time * 0.5 ) * d;
    this.light6.position.x = Math.cos( time * 0.7 ) * d;
    this.light6.position.z = Math.cos( time * 0.5 ) * d;

    this.renderer.render( this.scene, this.camera );
  
    };

  scene = () => {
    this.scene = new THREE.Scene()
    this.scene.background = new THREE.Color(0x040306);
    this.scene.fog = new THREE.Fog( 0x040306, 10, 300 );
  }

  light = () => {

    var intensity = 2.5;
    var distance = 100;
    var decay = 2.0;
    var c1 = 0xff0040, c2 = 0x0040ff, c3 = 0x80ff80, c4 = 0xffaa00, c5 = 0x00ffaa, c6 = 0xff1100;
    
    var sphere = new THREE.SphereBufferGeometry( 0.25, 16, 8 );

    this.light1 = new THREE.PointLight( c1, intensity, distance, decay );
    this.light1.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: c1 } ) ) );
    this.scene.add( this.light1 );
    this.light2 = new THREE.PointLight( c2, intensity, distance, decay );
    this.light2.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: c2 } ) ) );
    this.scene.add( this.light2 );
    this.light3 = new THREE.PointLight( c3, intensity, distance, decay );
    this.light3.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: c3 } ) ) );
    this.scene.add( this.light3 );
    this.light4 = new THREE.PointLight( c4, intensity, distance, decay );
    this.light4.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: c4 } ) ) );
    this.scene.add( this.light4 );
    this.light5 = new THREE.PointLight( c5, intensity, distance, decay );
    this.light5.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: c5 } ) ) );
    this.scene.add( this.light5 );
    this.light6 = new THREE.PointLight( c6, intensity, distance, decay );
    this.light6.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: c6 } ) ) );
    this.scene.add( this.light6 );
    var dlight = new THREE.DirectionalLight( 0xffffff, 0.05 );
    dlight.position.set( 0.5, 1, 0 ).normalize();
    this.scene.add( dlight );


  }

  camera = () => {
    this.camera = new THREE.PerspectiveCamera(75, 
      window.innerWidth / window.innerHeight, 
      1, 1000);

      this.camera.position.x = 100;
      this.camera.position.z = 100;
      this.camera.position.y = 50;
  }

  ground = () => {

    this.texture.repeat.set( 20, 10 );
    this.texture.wrapS = this.texture.wrapT = THREE.RepeatWrapping;
    this.texture.format = THREE.RGBFormat;

    var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, map: this.texture } );

    var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 800, 400, 2, 2 ), groundMaterial );
				mesh.position.y = - 5;
				mesh.rotation.x = - Math.PI / 2;
				this.scene.add( mesh );
  }

  controls = () => {

    this.controls = new FirstPersonControls( this.camera );
    this.controls.movementSpeed = 30;
    this.controls.lookSpeed = 0.1;
    this.controls.heightMin = 0;
    this.controls.verticalMin = 20;

  }

  componentDidMount() {

    //ADD SCENE
    this.scene();

    //ADD CAMERA
    this.camera();

    // ADD LIGHT
    this.light();

    // TEXTURES
    const objects = Objects();
    
    // example how you can import your functions
    objects.map((mesh) => {
      this.scene.add(mesh);
    })

    this.ground();

    this.controls();

    //ADD RENDERER
    this.renderer = new THREE.WebGLRenderer()
  
    this.renderer.setSize(window.innerWidth, window.innerHeight)
    this.renderer.setPixelRatio( window.devicePixelRatio ); // Sets device pixel ratio. This is usually used for HiDPI device to prevent bluring output canvas.
    this.mount.appendChild(this.renderer.domElement)

    this.renderer.gammaInput = true;
		this.renderer.gammaOutput = true;
    
    this.start()
    
  }

  componentWillUnmount() {
    this.stop()
    this.mount.removeChild(this.renderer.domElement)
  }

  start = () => {
    if (!this.frameId) {
      this.frameId = requestAnimationFrame(this.animate)
    }
  }

  componentWillMount(){
     let textureLoader = new THREE.TextureLoader();
     this.texture = textureLoader.load( require("./assets/disturb.jpg") );
  }

  stop = () => {
    cancelAnimationFrame(this.frameId)
  }

  animate = () => {

    this.mainLoop()
    this.renderScene()

    this.controls.update( clock.getDelta() );
    this.frameId = window.requestAnimationFrame(this.animate)
  }
  renderScene = () => {
    this.renderer.render(this.scene, this.camera)
  }
  
  render() {

//
    return (
      <div
        style={{ width: '400px', height: '400px' }}
        ref={(mount) => { this.mount = mount }}
      />
    )
  }
}
export default App